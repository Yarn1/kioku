-- Your SQL goes here
CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  user_id SERIAL UNIQUE,
  username TEXT UNIQUE NOT NULL,
  password TEXT NOT NULL,
  admin BOOLEAN NOT NULL DEFAULT false
);

CREATE INDEX "user_id" ON "public"."users"("user_id");
CREATE INDEX "username" ON "public"."users"("username");
