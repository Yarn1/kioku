-- Your SQL goes here
CREATE TABLE shares (
  id SERIAL PRIMARY KEY,
  -- user_id INT NOT NULL REFERENCES users(user_id),
  file_id TEXT NOT NULL REFERENCES files(external_id),
  share_path TEXT UNIQUE NOT NULL,
  share_time TIMESTAMP NOT NULL DEFAULT (now() at time zone 'utc'),
  valid_duration INTERVAL,
  login_required BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE INDEX "share_path" ON "public"."shares"("share_path");
-- CREATE INDEX "file_name" ON "public"."files"("file_name", "user_id");
-- CREATE INDEX "external_id" ON "public"."files"("external_id");

INSERT INTO "public"."users"("user_id", "username", "password")
VALUES(4, 'derp', '$rscrypt$0$EBAB$uO9Htx+9bN6bGviY1HFeEg==$xp56mBVNuLz3RPFQnU03JfibriIuWIPRHTZIKVAvBoo=$')
;--RETURNING "id", "user_id", "username", "password", "admin";

INSERT INTO "public"."files"("external_id", "store_path", "file_name", "mime_type", "upload_time", "user_id")
VALUES('61125d5a887b11e88000000000000000', 'x.png', 'x.png', 'image/png', '2018-07-15 22:07:10.121827', 4)
;--RETURNING "id", "external_id", "store_path", "file_name", "mime_type", "upload_time", "user_id";

INSERT INTO "public"."shares"("file_id", share_path, "share_time", "valid_duration")
VALUES('61125d5a887b11e88000000000000000', 'asdf', '2018-07-15 22:07:10.121827', NULL);
INSERT INTO "public"."shares"("file_id", share_path, "share_time", "valid_duration", "login_required")
VALUES('61125d5a887b11e88000000000000000', 'asdfa', '2018-07-15 22:07:10.121827', '30 days', TRUE);
;--RETURNING "id", "external_id", "store_path", "file_name", "mime_type", "upload_time", "user_id";
