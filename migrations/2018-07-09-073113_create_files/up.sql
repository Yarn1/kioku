-- Your SQL goes here
CREATE TABLE files (
  id SERIAL PRIMARY KEY,
  user_id INT NOT NULL REFERENCES users(user_id),
  external_id TEXT UNIQUE NOT NULL,
  store_path TEXT UNIQUE NOT NULL,
  file_name TEXT NOT NULL,
  mime_type TEXT,
  upload_time TIMESTAMP NOT NULL DEFAULT (now() at time zone 'utc')
);

CREATE INDEX "file_name" ON "public"."files"("file_name", "user_id");
CREATE INDEX "external_id" ON "public"."files"("external_id");

-- CREATE TABLE test (
--   id SERIAL PRIMARY KEY,
--   thing TEXT NOT NULL
-- );
-- {
--     "_id" : ObjectId("5978b23263373945282eebcc"),
--     "store_path" : "566a2fcc721511e78000000000000000",
--     "file_name" : "02.png",
--     "mime_type" : "image/png",
--     "user" : NumberLong(0),
--     "upload_time" : NumberLong(1501082162)
-- }


