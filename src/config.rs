use ron::de::from_reader;
use std::fs::File;

fn get_false() -> bool {
    false
}

fn get_none<T>() -> Option<T> {
    None
}

fn get_default_bind_addr() -> String {
    "127.0.0.1".to_string()
}

fn get_default_port() -> u16 {
    8000
}

#[derive(Clone, Debug, Deserialize)]
pub struct Config {
    pub db_url: String,
    pub store_path: String,
    pub jwt_secret: String,
    
    #[serde(default = "get_none")]
    pub static_path: Option<String>,
    
    #[serde(default = "get_default_bind_addr")]
    pub address: String,
    #[serde(default = "get_default_port")]
    pub port: u16,
    
    // unused
    
    #[serde(default = "get_false")]
    pub admin_upload: bool,
    
    #[serde(default = "get_none")]
    pub insecure_bind_addr: Option<String>,
    #[serde(default = "get_none")]
    pub tls_cert: Option<(String, String)>,
    #[serde(default = "get_false")]
    pub static_index: bool,
}

impl Config {
    pub fn from_ron_file(path: &str) -> Self {
        let file = File::open(path).expect("could not open config file");
        let config: Config = from_reader(file).unwrap();
        config
    }
}
