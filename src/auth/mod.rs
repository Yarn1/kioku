
use crypto::scrypt::{scrypt_simple, scrypt_check, ScryptParams};

pub mod rocket;

pub type UserId = u64;

fn get_false() -> bool {
    false
}

fn is_false(v: &bool) -> bool {
    !v
}

#[allow(dead_code)]
#[derive(Debug, Serialize, Deserialize)]
pub struct User {
    pub id: UserId,
    pub admin: bool,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Session {
    pub id: UserId,
    #[serde(default = "get_false", skip_serializing_if = "is_false")]
    pub admin: bool,
    #[serde(default = "get_false", skip_serializing_if = "is_false")]
    pub write: bool,
}

pub fn hash_password(password: &str) -> Result<String, ()> {
    let params = ScryptParams::new(16, 16, 1);
    let hash = scrypt_simple(password, &params).map_err(|_| ())?;
    Ok(hash)
}

pub fn check_password(password: &str, hash: &str) -> Result<bool, ()> {
    scrypt_check(password, hash).map_err(|_| ())
}
