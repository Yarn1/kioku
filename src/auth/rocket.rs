
use rocket::request::{self, FromRequest};
use rocket::{Request, Outcome};
use rocket::http::Status;
use rocket::http::Cookies;
use rocket::State;

use jwt::{decode, Validation};

use crate::config::Config;

use crate::auth;

pub struct Auth {
    pub user_id: u64,
    pub session: auth::Session,
}

fn get_header_auth<'a, 'r>(request: &'a Request<'r>) -> Option<&'a str> {
    let headers = request.headers();
    match headers.get_one("Authorization") {
        Some(value) => {
            // get the right part from Bearer: x
            let token = value.splitn(2, ':').nth(1)?;
            let token = token.trim();
            Some(token)
        }
        None => {
            None
        }
    }
}

fn get_cookie_auth<'a>(cookies: &'a Cookies) -> Option<&'a str> {
    
    let auth_cookie = match cookies.get("auth") {
        Some(cookie) => cookie,
        None => return None
    };
    let token = auth_cookie.value();
    Some(token)
}

impl<'a, 'r> FromRequest<'a, 'r> for Auth {
    type Error = ();
    
    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
        let cookies = request.cookies();
        let config = request.guard::<State<Config>>()?;
        let ref secret = config.jwt_secret;
        
        let token = match get_header_auth(&request).or_else(|| get_cookie_auth(&cookies)) {
            Some(token) => token,
            None => return Outcome::Failure((Status::Forbidden, ()))
        };
        // println!("{:?}", token);
        
        let mut validation = Validation::default();
        validation.validate_exp = false;
        validation.validate_iat = false;
        validation.validate_nbf = false;
        
        let token = match decode::<auth::Session>(token, secret.as_bytes(), &validation) {
            Ok(token) => { token },
            Err(_err) => {
                println!("token decode err {:?}", _err);
                
                return Outcome::Failure((Status::Forbidden, ()));
            }
        };
        
        let session = token.claims;
        
        Outcome::Success(Auth {
            user_id: session.id,
            session: session,
        })
    }
}

pub struct WriteAuth {
    auth: Auth
}

use std::ops::Deref;
impl Deref for WriteAuth {
    type Target = Auth;
    
    fn deref(&self) -> &Self::Target {
        &self.auth
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for WriteAuth {
    type Error = ();
    
    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
        let auth = request.guard::<Auth>()?;
        
        if !auth.session.write {
            return Outcome::Failure((Status::Forbidden, ()))
        }
        
        Outcome::Success(WriteAuth {
            auth: auth
        })
    }
}

pub struct AdminAuth {
    auth: Auth
}

impl Deref for AdminAuth {
    type Target = Auth;
    
    fn deref(&self) -> &Self::Target {
        &self.auth
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for AdminAuth {
    type Error = ();
    
    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
        let auth = request.guard::<WriteAuth>()?;
        
        if !auth.session.admin {
            return Outcome::Failure((Status::Forbidden, ()))
        }
        
        Outcome::Success(AdminAuth {
            auth: auth.auth
        })
    }
}
