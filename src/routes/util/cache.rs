
use rocket::response::{self, Response, Responder};
use rocket::request::Request;
use super::failable_responder::{FailableResponder, FailableResponse};

pub struct Cached<T> {
    res: T
}

impl<'r, T: Responder<'r>> Cached<T> {
    pub fn new(res: T) -> Self {
        Self {
            res: res
        }
    }
}

impl<'r, T: FailableResponder<'r>> Cached<T> {
    pub fn failable(res: T) -> Self {
        Self {
            res: res
        }
    }
}

impl<'r, T: Responder<'r>> Responder<'r> for Cached<T> {
    fn respond_to(self, request: &Request) -> response::Result<'r> {
        
        let mut response = Response::build_from(self.res.respond_to(request)?);
        
        response.raw_header("Cache-Control", "private, max-age=31536000");
        
        response.ok()
    }
}

impl <'r, T: FailableResponder<'r>> FailableResponder<'r> for Cached<T> {
    fn failable_respond_to(self, request: &Request) -> FailableResponse<'r> {
        let mut response = Response::build_from(self.res.failable_respond_to(request)?);
        
        response.raw_header("Cache-Control", "private, max-age=31536000");
        
        response.ok()
    }
}

