
use rocket::response::{Response, Responder};
use rocket::response;
use rocket::request::Request;
use rocket::http::Status;
use rocket::http::ContentType;
use std::io::Cursor;

pub type FailableResponse<'r> = Result<Response<'r>, (Status, String)>;

pub trait FailableResponder<'r> {
    fn failable_respond_to(self, request: &Request) -> FailableResponse<'r>;
}

pub struct FailableWrapper<T> {
    inner: T,
}

impl<'r, T: FailableResponder<'r>> FailableWrapper<T> {
    pub fn new(inner: T) -> Self {
        Self {
            inner: inner,
        }
    }
}

impl<'r, T: FailableResponder<'r>> Responder<'r> for FailableWrapper<T> {
    fn respond_to(self, request: &Request) -> response::Result<'r> {
        let f_resp = self.inner.failable_respond_to(request);
        
        match f_resp {
            Ok(resp) => {
                Ok(resp)
            }
            Err((status, msg)) => {
                Response::build()
                    .status(status)
                    .header(ContentType::Plain)
                    .sized_body(Cursor::new(msg.into_bytes()))
                    .ok()
            }
        }
    }
}
