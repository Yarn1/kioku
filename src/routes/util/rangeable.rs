
use std::fs::File;

use rocket::response::Stream;
use rocket::http::Status;

use rocket::response::{Response, Responder};
use rocket::request::Request;
use std::io::Read;
use std::io::Seek;
use std::io;

pub trait RangeableFile
    where Self: Read + Seek {
    
    fn get_size(&self) -> io::Result<u64>;
}

impl RangeableFile for File {
    fn get_size(&self) -> io::Result<u64> {
        let metadata = self.metadata()?;
        let file_len = metadata.len();
        Ok(file_len)
    }
}

pub struct RangedFile<T> {
    file: T
}

impl<T: RangeableFile> RangedFile<T> {
    pub fn new(file: T) -> Self {
        Self { file: file }
    }
}

use super::failable_responder::{FailableResponder, FailableResponse};
impl <'r, T: RangeableFile + 'r> FailableResponder<'r> for RangedFile<T> {
    fn failable_respond_to(self, request: &Request) -> FailableResponse<'r> {
        
        let headers = request.headers();
        
        use rocket::http::hyper::header::Range;
        use rocket::http::hyper::header::ByteRangeSpec;
        use rocket::http::hyper::header::ContentRange;
        use rocket::http::hyper::header::ContentRangeSpec;
        use rocket::http::hyper::header::ContentLength;
        use std::io::SeekFrom;
        use std::str::FromStr;
        
        let ranges: Option<Range> = match headers.get_one("Range").map(|range| Range::from_str(range)) {
            Some(Ok(range)) => Some(range),
            Some(Err(err)) => {
                return Err((Status::BadRequest, format!("bad range header: {}", err)))
            }
            None => None,
        };
        
        let mut response = match ranges {
            Some(Range::Bytes(ref ranges)) => {
                let range = &ranges[0];
                let mut response = match range {
                    ByteRangeSpec::AllFrom(ref start) => {
                        let mut file = self.file;
                        let file_len = file.get_size().unwrap();
                        
                        file.seek(SeekFrom::Start(*start)).unwrap();
                        let stream = Stream::from(file);
                        let range_spec = ContentRangeSpec::Bytes {
                            range: Some((*start, file_len - 1)),
                            instance_length: Some(file_len),
                        };
                        
                        let res = stream.respond_to(request).map_err(|status| (status, "".into()))?;
                        let mut response = Response::build_from(res);
                        
                        response.header(ContentLength(file_len - start));
                        response.header(ContentRange(range_spec));
                        response
                    }
                    ByteRangeSpec::FromTo(ref start, ref end) => {
                        let mut file = self.file;
                        let file_len = file.get_size().unwrap();
                        
                        file.seek(SeekFrom::Start(*start)).unwrap();
                        let file = file.take(end-start+1);
                        
                        let range_spec = ContentRangeSpec::Bytes {
                            range: Some((*start, *end)),
                            instance_length: Some(file_len),
                        };
                        
                        let stream = Stream::from(file);
                        
                        let res = stream.respond_to(request).map_err(|status| (status, "".into()))?;
                        let mut response = Response::build_from(res);
                        
                        response.header(ContentLength(end-start+1));
                        response.header(ContentRange(range_spec));
                        response
                    }
                    ByteRangeSpec::Last(ref last_n) => {
                        
                        let mut file = self.file;
                        let file_len = file.get_size().unwrap();
                        
                        let start = file_len - last_n;
                        
                        file.seek(SeekFrom::Start(start)).unwrap();
                        
                        let range_spec = ContentRangeSpec::Bytes {
                            range: Some((start, file_len-1)),
                            instance_length: Some(file_len),
                        };
                        
                        let stream = Stream::from(file);
                        
                        let res = stream.respond_to(request).map_err(|status| (status, "".into()))?;
                        let mut response = Response::build_from(res);
                        
                        response.header(ContentLength(*last_n));
                        response.header(ContentRange(range_spec));
                        response
                    }
                };
                response.status(Status::PartialContent);
                response
            }
            Some(Range::Unregistered(_, _)) => {
                return Err((Status::BadRequest, "unsupported range unit".into()))
            }
            None => {
                let file = self.file;
                let file_len = file.get_size().unwrap();
                
                let stream = Stream::from(file);
                
                let res = stream.respond_to(request).map_err(|status| (status, "".into()))?;
                let mut response = Response::build_from(res);
                
                response.header(ContentLength(file_len));
                response
            }
        };
        
        response.ok()
    }
}
