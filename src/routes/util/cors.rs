
use rocket::response::{self, Response, Responder};
use rocket::request::Request;
use super::failable_responder::{FailableResponder, FailableResponse};
use rocket::http::hyper::header::AccessControlAllowOrigin;
use rocket::http::hyper::header::AccessControlAllowHeaders;

pub struct AllowOrigin<T> {
    res: T
}

impl<'r, T: Responder<'r>> AllowOrigin<T> {
    pub fn new(res: T) -> Self {
        Self {
            res: res
        }
    }
}

impl<'r, T: FailableResponder<'r>> AllowOrigin<T> {
    pub fn failable(res: T) -> Self {
        Self {
            res: res
        }
    }
}

impl<'r, T: Responder<'r>> Responder<'r> for AllowOrigin<T> {
    fn respond_to(self, request: &Request) -> response::Result<'r> {
        let allow_origin = AccessControlAllowOrigin::Any;
        let allow_headers = AccessControlAllowHeaders(vec!["Authorization".into()]);
        
        let mut response = Response::build_from(self.res.respond_to(request)?);
        response.header(allow_origin);
        response.header(allow_headers);
        
        response.ok()
    }
}

impl<'r, T: FailableResponder<'r>> FailableResponder<'r> for AllowOrigin<T> {
    fn failable_respond_to(self, request: &Request) -> FailableResponse<'r> {
        let allow_origin = AccessControlAllowOrigin::Any;
        let allow_headers = AccessControlAllowHeaders(vec!["Authorization".into()]);
        
        let mut response = Response::build_from(self.res.failable_respond_to(request)?);
        response.header(allow_origin);
        response.header(allow_headers);
        
        response.ok()
    }
}
