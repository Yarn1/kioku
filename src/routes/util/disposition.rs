
use rocket::response::{self, Response, Responder};
use rocket::request::Request;
use super::failable_responder::{FailableResponder, FailableResponse};

pub struct Disposition<T> {
    file_name: String,
    res: T
}

impl<'r, T: Responder<'r>> Disposition<T> {
    pub fn new(file_name: String, res: T) -> Self {
        Self {
            file_name: file_name,
            res: res,
        }
    }
}

impl<'r, T: FailableResponder<'r>> Disposition<T> {
    pub fn failable(file_name: String, res: T) -> Self {
        Self {
            file_name: file_name,
            res: res,
        }
    }
}

impl<'r, T: Responder<'r>> Responder<'r> for Disposition<T> {
    fn respond_to(self, request: &Request) -> response::Result<'r> {
        use rocket::http::hyper::header::{ContentDisposition, DispositionType, DispositionParam, Charset};
        
        let content_disposition = ContentDisposition {
            disposition: DispositionType::Inline,
            parameters: vec![DispositionParam::Filename(
                Charset::Ext("UTF-8".to_string()),
                None,
                self.file_name.as_bytes().to_vec()
            )]
        };
        
        let mut response = Response::build_from(self.res.respond_to(request)?);
        
        response.header(content_disposition);
        
        response.ok()
    }
}

impl<'r, T: FailableResponder<'r>> FailableResponder<'r> for Disposition<T> {
    fn failable_respond_to(self, request: &Request) -> FailableResponse<'r> {
        use rocket::http::hyper::header::{ContentDisposition, DispositionType, DispositionParam, Charset};
        
        let content_disposition = ContentDisposition {
            disposition: DispositionType::Inline,
            parameters: vec![DispositionParam::Filename(
                Charset::Ext("UTF-8".to_string()),
                None,
                self.file_name.as_bytes().to_vec()
            )]
        };
        
        let mut response = Response::build_from(self.res.failable_respond_to(request)?);
        
        response.header(content_disposition);
        
        response.ok()
    }
}
