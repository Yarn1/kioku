
use rocket::response::{Response, Responder};
use rocket::request::Request;
use rocket::http::ContentType;
use rocket::http::Status;
use super::failable_responder::{FailableResponder, FailableResponse};

#[derive(Debug, Clone, PartialEq)]
pub struct Content<R> {
    content_type: ContentType,
    res: R,
}

impl<'r, R: Responder<'r>> Content<R> {
    pub fn new(content_type: ContentType, res: R) -> Self {
        Self {
            content_type: content_type,
            res: res,
        }
    }
}

impl<'r, R: FailableResponder<'r>> Content<R> {
    pub fn failable(content_type: ContentType, res: R) -> Self {
        Self {
            content_type: content_type,
            res: res,
        }
    }
}

impl<'r, R: Responder<'r>> Responder<'r> for Content<R> {
    #[inline(always)]
    fn respond_to(self, req: &Request) -> Result<Response<'r>, Status> {
        Response::build()
            .merge(self.res.respond_to(req)?)
            .header(self.content_type)
            .ok()
    }
}

impl<'r, R: FailableResponder<'r>> FailableResponder<'r> for Content<R> {
    #[inline(always)]
    fn failable_respond_to(self, req: &Request) -> FailableResponse<'r> {
        Response::build()
            .merge(self.res.failable_respond_to(req)?)
            .header(self.content_type)
            .ok()
    }
}
