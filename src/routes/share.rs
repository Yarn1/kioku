
use std;
use std::str::FromStr;

use rocket::State;
use rocket::response::Responder;
use rocket::http::Status;
use rocket::http::uri::Segments;
use rocket::http::ContentType;
use rocket::response::status::Custom;

use crate::auth::rocket::{Auth, WriteAuth};
use crate::database::Backend;
use crate::storage_fs::FsFileStore;

use super::util::Content;
use super::util::RangedFile;
use super::util::AllowOrigin;
use super::util::Cached;
use super::util::Disposition;
use super::util::failable_responder::FailableWrapper;


#[get("/s/<share_path..>")]
pub fn get_share<'r>(
        mut share_path: Segments,
        auth: Option<Auth>,
        file_store: State<FsFileStore>,
        backend: Backend) -> impl Responder<'r> {
    
    let share_path = share_path.next().expect("no segments");
    
    let share = backend.get_share(share_path).expect("failed to load share from db");
    
    if share.login_required && auth.is_none() {
        println!("must be logged in to view share");
        return Err(Status::Forbidden);
    }
    
    let mut path = ::std::path::PathBuf::new();
    path.push(&file_store.path);
    path.push(share.store_path);
    
    let file = std::fs::File::open(path).expect("couldn't load file from disk");
    let stream = RangedFile::new(file);
    
    let content_type = share.mime_type.unwrap_or_default();
    
    let ranged_file = stream;
    
    let resp = Content::failable(ContentType::from_str(&content_type).unwrap_or_default(), ranged_file);
    
    let resp = Cached::failable(resp);
    let resp = Disposition::failable(share.file_name, resp);
    
    Ok(FailableWrapper::new(resp))
}

#[options("/create_share/<_file_id>")]
pub fn create_share_options(_file_id: String) -> AllowOrigin<()> {
    AllowOrigin::new(())
}

#[get("/create_share/<file_id>")]
pub fn create_share(file_id: String, auth: WriteAuth, backend: Backend)
        -> AllowOrigin<Result<Option<String>, Custom<Option<&'static str>>>> {
    
    let share_path = backend.create_share(auth.user_id, &file_id).expect("failed to create share");
    
    AllowOrigin::new(Ok(Some(share_path)))
}
