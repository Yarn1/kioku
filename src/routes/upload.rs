
use mime_guess::guess_mime_type_opt;
use multipart::server::Multipart;

use rocket::Data;
use rocket::http::ContentType;
use rocket::response::status::Custom;
use rocket::http::Status;
use rocket::State;

use super::util::AllowOrigin;
use crate::models;
use crate::auth::rocket::WriteAuth;
use crate::database::Backend;
use crate::storage_fs::FsFileStore;


#[options("/upload")]
pub fn upload_options() -> AllowOrigin<()> {
    AllowOrigin::new(())
}

#[post("/upload", format="multipart/form-data", data="<data>", rank=3)]
pub fn upload_test_1<'a>(
    auth: WriteAuth, data: Data, cont_type: &ContentType, file_store: State<FsFileStore>, backend: Backend)
        -> AllowOrigin<Result<&'a str, Custom<String>>> {
    //https://github.com/abonander/multipart/blob/master/examples/rocket.rs
    
    let boundary = match cont_type.params().find(|&(k, _)| k == "boundary") {
        Some((_, boundary)) => boundary,
        None => {
            return AllowOrigin::new(Err(
                Custom(
                    Status::BadRequest,
                    "`Content-Type: multipart/form-data` boundary param not provided".into()
                )
            ));
        }
    };
    
    let mut file_uploaded = false;
    
    let mut multipart = Multipart::with_body(data.open(), boundary);
    let multipart_result = multipart.foreach_entry(|field| {
        
        let headers = field.headers;
        let field_name = headers.name;
        let file_name = match headers.filename {
            Some(filename) => filename,
            None => return,
        };
        
        let content_type = match headers.content_type {
            Some(content_type) => Some(format!("{}", content_type)),
            None => guess_mime_type_opt(&file_name).map(|mime| format!("{}", mime)),
        };
        let content_type_str = content_type.as_ref().map(|s| s.as_str());
        
        if &*field_name != "file" {
            return;
        }
        
        let mut data = field.data;
        let fs_file = file_store.create_file();
        let temp = data.save().size_limit(None).write_to(&fs_file.file);
        // println!("save res {:?}", temp);
        temp.into_result_strict().unwrap();
        
        let new_file = models::NewFile {
            external_id: &fs_file.store_path,
            store_path: &fs_file.store_path,
            file_name: &file_name,
            mime_type: content_type_str,
            user_id: auth.user_id as i32,
        };
        
        let insert_result = backend.create_file(new_file);
        match insert_result {
            Ok(_) => {
                file_uploaded = true;
            }
            Err(_) => {
                panic!("error writing file to db");
                // (Status::ServiceUnavailable, ())
            }
        }
    });
    if let Err(_) = multipart_result {
        println!("multipart error");
        return AllowOrigin::new(Err(Custom(
            Status::BadRequest,
            "Could not parse multipart data".into()
        )));
    }
    
    if file_uploaded {
        AllowOrigin::new(Ok("upload complete"))
    } else {
        AllowOrigin::new(Err(Custom(
            Status::BadRequest,
            "no file to upload, need field 'file' with filename attribute".into()
        )))
    }
}

#[post("/upload/<file_name>", data="<data>", rank=4)]
pub fn upload_test_2<'a>(
    file_name: String, data: Data, content_type: Option<&ContentType>, file_store: State<FsFileStore>, auth: WriteAuth, backend: Backend)
        -> AllowOrigin<Result<&'a str, Custom<String>>> {
    let mut fs_file = file_store.create_file();
    data.stream_to(&mut fs_file.file).unwrap();
    
    let content_type = match content_type {
        Some(content_type) => Some(format!("{}", content_type)),
        None => guess_mime_type_opt(&file_name).map(|mime| format!("{}", mime)),
    };
    let content_type_str = content_type.as_ref().map(|s| s.as_str());
    
    let new_file = models::NewFile {
        external_id: &fs_file.store_path,
        store_path: &fs_file.store_path,
        file_name: &file_name,
        mime_type: content_type_str,
        user_id: auth.user_id as i32,
    };
    
    backend.create_file(new_file).expect("error saving new file to db");
    AllowOrigin::new(Ok("ok"))
}
