
use rocket::response::status::Custom;
use rocket::http::Status;
use rocket::http::{Cookies, Cookie};
use rocket::State;
use rocket::response::Responder;
use rocket_contrib::json::Json;

use jwt::{encode, Header};
use time::Duration;

use crate::auth;
use crate::auth::rocket::{Auth, WriteAuth, AdminAuth};
use crate::database::Backend;
use crate::database::models::{User, NewUser, UpdateUser};
use crate::database::BackendError;
use crate::config::Config;

#[get("/hash/<password>")]
pub fn hash_pass(password: String) -> String {
    auth::hash_password(&password).unwrap_or("failed".to_string())
}

#[get("/user_id")]
pub fn get_user_id(auth: Auth) -> String {
    auth.user_id.to_string()
}

#[get("/set_cookie")]
pub fn make_token(auth: WriteAuth, mut cookies: Cookies, config: State<Config>) -> String {
    let session = auth::Session {
        id: auth.user_id,
        write: false,
        admin: false,
    };
    
    let ref secret = config.jwt_secret;
    let token = encode(&Header::default(), &session, secret.as_bytes()).unwrap();
    
    let cookie: Cookie = Cookie::build("auth", token)
        .path("/")
        .http_only(true)
        .secure(config.tls_cert != None)
        .max_age(Duration::weeks(1))
        .finish();
    cookies.add(cookie);
    
    "ok".into()
}

#[post("/login", data="<data>")]
pub fn login(data: String, mut cookies: Cookies, backend: Backend, config: State<Config>) -> Result<String, Custom<String>> {
    let mut data = data.split("\n");
    let username = data.next().unwrap();
    let password = data.next().unwrap();
    
    let user = backend.get_user(username).expect("Error loading user from db");
    
    let password_ok = auth::check_password(password, user.password.as_str()).unwrap_or(false);
    
    if password_ok {
        let mut session = auth::Session {
            id: user.user_id as u64,
            write: false,
            admin: user.admin,
        };
        
        let ref secret = config.jwt_secret;
        let token = encode(&Header::default(), &session, secret.as_bytes()).unwrap();
        
        let cookie: Cookie = Cookie::build("auth", token)
            .path("/")
            .http_only(true)
            .secure(config.tls_cert != None)
            .max_age(Duration::weeks(1))
            .finish();
        cookies.add(cookie);
        
        // return write token, set non write token in cookie
        session.write = true;
        let token = encode(&Header::default(), &session, secret.as_bytes()).unwrap();
        Ok(token)
    } else {
        Err(Custom(Status::Forbidden, "".into()))
    }
}

#[derive(Serialize, Deserialize)]
struct ReturnUser {
    user_id: i32,
    username: String,
    admin: bool,
}

impl From<User> for ReturnUser {
    fn from(user: User) -> Self {
        Self {
            user_id: user.user_id,
            username: user.username,
            admin: user.admin,
        }
    }
}

#[get("/get_users")]
pub fn get_users(_auth: AdminAuth, backend: Backend) -> impl Responder<'static> {
    match backend.get_users() {
        Ok(users) => {
            let users: Vec<_> = users.into_iter().map(|u| ReturnUser::from(u)).collect();
            Ok(Json(users))
        }
        Err(err) => {
            dbg!(err);
            Err("Unknown error")
        }
    }
}

#[post("/create_user", format="json", data="<user>")]
pub fn create_user(_auth: AdminAuth, user: Json<NewUser>, backend: Backend) -> impl Responder<'static> {
    let mut user: NewUser = user.clone();
    let password = auth::hash_password(user.password.to_mut()).unwrap();
    user.password = password.into();
    
    match backend.create_user(user) {
        Ok(_) => {
            Ok(())
        }
        Err(BackendError::AlreadyExists) => {
            Err("User already exists")
        }
        Err(err) => {
            dbg!(err);
            Err("Unknown error")
        }
    }
}

#[post("/update_user", format="json", data="<user>")]
pub fn update_user(_auth: AdminAuth, mut user: Json<UpdateUser>, backend: Backend) -> impl Responder<'static> {
    if let Some(ref mut password) = user.password {
        *password = auth::hash_password(password.to_mut()).unwrap().into();
    }
    
    match backend.update_user(user.clone()) {
        Ok(_) => {
            Ok(())
        }
        Err(BackendError::NotFound) => {
            Err("User not found")
        }
        Err(err) => {
            dbg!(err);
            Err("Unkown error")
        }
    }
}

#[derive(Deserialize)]
pub struct DeleteUser {
    user_id: u64,
}

#[post("/delete_user", format="json", data="<user>")]
pub fn delete_user(_auth: AdminAuth, user: Json<DeleteUser>, backend: Backend) -> impl Responder<'static> {
    match backend.delete_user(user.user_id) {
        Ok(()) => {
            Ok(())
        }
        Err(BackendError::NotFound) => {
            Err("User not found")
        }
        Err(err) => {
            dbg!(err);
            Err("Unkown error")
        }
    }
}
