
use std;

use rocket::State;
use rocket::response::Responder;

use crate::auth::rocket::Auth;
use crate::database::Backend;
use crate::storage_fs::FsFileStore;

use super::util::Content;
use super::util::RangedFile;
use super::util::AllowOrigin;
use super::util::Cached;
use super::util::Disposition;
use super::util::failable_responder::FailableWrapper;

#[options("/list")]
pub fn get_file_list_options() -> AllowOrigin<()> {
    AllowOrigin::new(())
}

#[get("/list")]
pub fn get_file_list(auth: Auth, backend: Backend) -> AllowOrigin<String> {
    
    let result = backend.get_files(auth.user_id).expect("Error loading file");
    
    let mut output = String::new();
    
    for file in result {
        output.push_str(&file.external_id);
        output.push('|');
        // extra metadata here
        if let Some(mime_type) = file.mime_type {
            output.push_str("mime_type:");
            output.push_str(&mime_type);
            output.push(',');
        }
        output.push('|');
        output.push_str(&file.file_name);
        output.push('\n');
    }
    
    AllowOrigin::new(output)
}

#[get("/get/<file_name>")]
pub fn get_file_1<'r>(file_name: String, file_store: State<FsFileStore>, auth: Auth, backend: Backend) -> impl Responder<'r> {
    
    let result = backend.get_file_by_name(auth.user_id, &file_name).expect("Error loading file");
    
    let mut path = ::std::path::PathBuf::new();
    path.push(&file_store.path);
    path.push(result.store_path);
    
    let file = std::fs::File::open(path).unwrap();
    
    use rocket::http::ContentType;
    use std::str::FromStr;
    
    let content_type = result.mime_type.unwrap_or("application/octet-stream".parse().unwrap());
    
    let ranged_file = RangedFile::new(file);
    
    let resp = Content::failable(ContentType::from_str(&content_type).unwrap(), ranged_file);
    
    let resp = Cached::failable(resp);
    let resp = Disposition::failable(result.file_name, resp);
    
    FailableWrapper::new(resp)
}
