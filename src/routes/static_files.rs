
use rocket::response::NamedFile;
use rocket::request::Request;
use std::path::{Path, PathBuf};
use rocket::response::{self, Response, Responder};
use rocket::request::State;

use crate::config::Config;


pub struct StaticFile {
    file: NamedFile
}

impl StaticFile {
    fn new(file: NamedFile) -> Self {
        Self {
            file: file,
        }
    }
}

impl<'r> Responder<'r> for StaticFile {
    fn respond_to(self, request: &Request) -> response::Result<'r> {
        Response::build_from(self.file.respond_to(request)?)
            .raw_header("Cache-Control", "private, max-age=31536000")
            .ok()
    }
}

#[get("/")]
pub fn files_index(config: State<Config>) -> Option<StaticFile> {
    let static_path = config.static_path.as_ref()?;
    
    let path = Path::new(static_path).join("index.html");
    
    let file = NamedFile::open(path).ok()?;
    
    Some(StaticFile::new(file))
}

#[get("/<file_path..>", rank = 10)]
pub fn files_ui(file_path: PathBuf, config: State<Config>) -> Option<StaticFile> {
    let static_path = config.static_path.as_ref()?;
    
    let mut path = Path::new(static_path).join(&file_path);
    if path.is_dir() {
        path = Path::new(static_path).join(&file_path).join("index.html");
    }
    
    let file = NamedFile::open(path).ok()?;
    
    Some(StaticFile::new(file))
}
