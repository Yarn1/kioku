
#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
extern crate rocket_contrib;
#[cfg(feature = "postgres")]
#[macro_use] extern crate diesel;
#[cfg(feature = "postgres")]
#[macro_use] extern crate diesel_migrations;
extern crate uuid;
extern crate time;
extern crate multipart;
extern crate crypto;
extern crate serde;
#[macro_use] extern crate serde_derive;
extern crate jsonwebtoken as jwt;
extern crate mime_guess;
extern crate ron;
extern crate serde_json;


pub mod database;
pub mod storage_fs;
pub mod auth;
pub mod config;
pub mod routes;

use rocket::config::{Config, Environment};

use database::models;
use database::BackendProvider;
use database::local::LocalBackend;

use routes::upload;
use routes::download;
use routes::auth as auth_route;
use routes::share;
use routes::static_files;

fn main() {
    let user_config = crate::config::Config::from_ron_file("./config.ron");
    
    if let Some(_) = ::std::env::args().find(|a| a == "--migrations" || a == "-m") {
        #[cfg(feature = "postgres")]
        {
            database::postgres::migration::run_migrations(&user_config.db_url);
        }
        return;
    }
    
    let file_store = storage_fs::FsFileStore::new(user_config.store_path.clone());
    
    let rocket_config = Config::build(Environment::Development)
        .address(user_config.address.clone())
        .port(user_config.port)
        .unwrap();
    
    let backend = if let Some(_) = ::std::env::args().find(|a| a == "--pg" || a == "-d") {
        #[cfg(feature = "postgres")]
        {
            use database::postgres::PostgresBackend;
            use database::postgres::init_pool;
            let pg_backend = PostgresBackend {
                file_store: storage_fs::FsFileStore::new(user_config.store_path.clone()),
                pool: init_pool(&user_config.db_url),
            };
            let backend = Box::new(pg_backend) as Box<BackendProvider>;
            backend
        }
        #[cfg(not(feature = "postgres"))]
        {
            panic!("binary was compiled without postgres feature")
        }
    } else {
        let backend_file_store = storage_fs::FsFileStore::new(user_config.store_path.clone());
        let mut local_backend = LocalBackend::new(backend_file_store, "./kioku_data.json".into());
        local_backend.load();
        let backend = Box::new(local_backend) as Box<BackendProvider>;
        backend
    };
    
    if let Some(_) = ::std::env::args().find(|a| a == "--init") {
        let password = "admin";
        let user = models::NewUser {
            username: "admin",
            password: auth::hash_password(password).unwrap().into(),
            admin: true,
        };
        
        backend.create_user(user.clone()).unwrap();
        println!("{:?}", user);
        println!("password: {:?}", password);
        
        return;
    }
    
    rocket::custom(rocket_config)
        .mount("/", routes![
            upload::upload_test_1, upload::upload_test_2, upload::upload_options,
            download::get_file_1,
            share::get_share,
            share::create_share, share::create_share_options,
            download::get_file_list, download::get_file_list_options,
            static_files::files_index, static_files::files_ui,
        ])
        .mount("/auth_test", routes![auth_route::hash_pass, auth_route::get_user_id, auth_route::make_token, auth_route::login])
        .mount("/users", routes![auth_route::get_users, auth_route::create_user, auth_route::update_user, auth_route::delete_user])
        .manage(file_store)
        .manage(user_config)
        .manage(backend)
        .launch();
}
