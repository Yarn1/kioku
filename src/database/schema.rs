table! {
    files (id) {
        id -> Int4,
        user_id -> Int4,
        external_id -> Text,
        store_path -> Text,
        file_name -> Text,
        mime_type -> Nullable<Text>,
        upload_time -> Timestamp,
    }
}

table! {
    shares (id) {
        id -> Int4,
        file_id -> Text,
        share_path -> Text,
        share_time -> Timestamp,
        valid_duration -> Nullable<Interval>,
        login_required -> Bool,
    }
}

table! {
    users (id) {
        id -> Int4,
        user_id -> Int4,
        username -> Text,
        password -> Text,
        admin -> Bool,
    }
}

allow_tables_to_appear_in_same_query!(
    files,
    shares,
    users,
);
