// workaround to silence warning in embed_migrations macro
#![allow(unused_imports)]

embed_migrations!("./migrations");

use diesel::Connection;
use diesel;

pub fn run_migrations(db_url: &str) {
    let conn = diesel::pg::PgConnection::establish(db_url).unwrap();
    embedded_migrations::run_with_output(&conn, &mut ::std::io::stdout()).unwrap();
}
