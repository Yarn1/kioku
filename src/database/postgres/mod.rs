
pub mod migration;

use std::ops::Deref;
use std::borrow::Cow;

use diesel;
use diesel::prelude::*;
use crate::database::schema::{files, shares, users};

// use models;
use crate::models::{File, NewFile, Share, NewShare, User, NewUser, UpdateUser};

use super::BackendProvider;
use super::BackendError;
use crate::storage_fs::FsFileStore;

use diesel::pg::PgConnection;
use diesel::r2d2::{ConnectionManager, Pool, PooledConnection};

use rocket::http::Status;
use rocket::request::{self, FromRequest};
use rocket::{Request, State, Outcome};

type PgPool = Pool<ConnectionManager<PgConnection>>;

pub fn init_pool(database_url: &str) -> PgPool {
    let manager = ConnectionManager::<PgConnection>::new(database_url);
    Pool::new(manager).expect("db pool")
}

pub struct DbConn(pub PooledConnection<ConnectionManager<PgConnection>>);

impl<'a, 'r> FromRequest<'a, 'r> for DbConn {
    type Error = ();
    
    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
        let pool = request.guard::<State<PgPool>>()?;
        match pool.get() {
            Ok(conn) => Outcome::Success(DbConn(conn)),
            Err(_) => Outcome::Failure((Status::ServiceUnavailable, ()))
        }
    }
}

impl Deref for DbConn{
    type Target = PgConnection;
    
    fn deref(&self) -> &Self::Target {
         &self.0
    }
}

#[derive(AsChangeset)]
#[table_name = "users"]
struct UpdateUserPg<'a> {
    pub username: Option<&'a str>,
    pub password: Option<Cow<'a, str>>,
    pub admin: Option<bool>,
}

impl<'a> From<UpdateUser<'a>> for UpdateUserPg<'a> {
    fn from(other: UpdateUser<'a>) -> Self {
        Self {
            username: other.username,
            password: other.password,
            admin: other.admin,
        }
    }
}

pub struct PostgresBackend {
    pub file_store: FsFileStore,
    pub pool: PgPool,
}

impl From<diesel::result::Error> for BackendError {
    fn from(err: diesel::result::Error) -> Self {
        use diesel::result::Error;
        match err {
            Error::NotFound => {
                BackendError::NotFound
            }
            _ => {
                BackendError::Other
            }
        }
    }
}

impl<'a> BackendProvider for PostgresBackend {
    fn get_file_by_name(&self, user_id: u64, file_name: &str) -> Result<File, BackendError> {
        let conn = self.pool.get().expect("failed to get connection from pool");
        
        let filter = files::file_name.eq(&file_name)
            .and(files::user_id.eq(user_id as i32));
        
        let result = files::table
            .filter(filter)
            .order(files::upload_time.desc())
            .first::<File>(&*conn)
            .expect("Error loading file from db");
        
        Ok(result)
    }
    
    fn get_files(&self, user_id: u64) -> Result<Vec<File>, BackendError> {
        let conn = self.pool.get().expect("failed to get connection from pool");
        
        let filter = files::user_id.eq(user_id as i32);
        
        let result = files::table
            .filter(filter)
            .order(files::upload_time.desc())
            .load::<File>(&*conn)
            .expect("Error loading file from db");
        
        Ok(result)
    }
    
    fn get_share(&self, share_path: &str) -> Result<Share, BackendError> {
        let conn = self.pool.get().expect("failed to get connection from pool");
        
        let filter = shares::share_path.eq(&share_path);
        
        let query = shares::table
            .inner_join(files::table.on(
                files::external_id.eq(shares::file_id)
            ))
            .filter(filter)
            .select((files::store_path, files::file_name, files::mime_type, shares::login_required));
        
        // let share = match query.first::<Share>(&*conn) {
        //     Ok(share) => share,
        //     Err(diesel::result::Error::NotFound) => return Err(None),
        //     Err(_) => {
        //         // Outcome::Failure((Status::ServiceUnavailable, ()))
        //         return Err(Some(Custom(
        //             Status::ServiceUnavailable,
        //             None
        //         )));
        //     }
        // };
        
        let share = query.first::<Share>(&*conn).expect("loading share from db failed");
        
        Ok(share)
    }
    
    fn create_file(&self, new_file: NewFile) -> Result<(), BackendError> {
        let conn = self.pool.get().expect("failed to get connection from pool");
        
        // let new_file = models::NewFile {
        //     external_id: &fs_file.store_path,
        //     store_path: &fs_file.store_path,
        //     file_name: &file_name,
        //     mime_type: content_type_str,
        //     user_id: auth.user_id as i32,
        // };
        diesel::insert_into(files::table)
            .values(new_file)
            // .get_result(&*conn)
            .execute(&*conn)
            .expect("error saving new file to db");
        
        Ok(())
    }
    
    fn create_share(&self, user_id: u64, file_id: &str) -> Result<String, BackendError> {
        let conn = self.pool.get().expect("failed to get connection from pool");
        
        let filter = files::user_id.eq(user_id as i32).and(
            files::external_id.eq(file_id)
        );
        
        let query = files::table
            // .inner_join(users::table.on(
            //     users::user_id.eq(files::user_id)
            // ))
            .filter(filter)
            .select(files::external_id);
        
        // let file_id = query.first::<String>(&*conn).unwrap();
        // let file_id = match query.first::<String>(&*conn) {
        //     Ok(file_id) => file_id,//share,
        //     Err(diesel::result::Error::NotFound) => return AllowOrigin::new(Ok(None)),
        //     Err(_) => {
        //         // ()
        //         // Outcome::Failure((Status::ServiceUnavailable, ()))
        //         return AllowOrigin::new(Err(Custom(Status::ServiceUnavailable, None)));
        //     }
        // };
        let file_id = query.first::<String>(&*conn).expect("create_share: failed to load file from db");
        let share_path = self.file_store.generate_uuid();
        
        {
            let new_share = NewShare {
                file_id: &file_id,
                share_path: &share_path,
                valid_duration: None,
                login_required: false,
            };
            let insert_result = diesel::insert_into(shares::table)
                .values(new_share)
                // .get_result(&*conn)
                .execute(&*conn);
            match insert_result {
                Ok(_) => {
                    // file_uploaded = true;
                }
                Err(_) => {
                    println!("goof intensifies");
                    panic!("error writing share to db");
                    // (Status::ServiceUnavailable, ())
                }
            }
        }
        
        Ok(share_path)
    }
    
    fn get_user(&self, username: &str) -> Result<User, BackendError> {
        let conn = self.pool.get().expect("failed to get connection from pool");
        
        let filter = users::username.eq(&username);
        let user = users::table
            .filter(filter)
            .first::<User>(&*conn)
            .expect("Error loading user from db");
        
        Ok(user)
    }
    
    fn get_users(&self) -> Result<Vec<User>, BackendError> {
        let conn = self.pool.get().expect("failed to get connection from pool");
        
        let users = users::table
            .load::<User>(&*conn)
            .expect("Failed loading users from db");
        
        Ok(users)
    }
    
    fn create_user(&self, user: NewUser) -> Result<(), BackendError> {
        let conn = self.pool.get().expect("failed to get connection from pool");
        
        diesel::insert_into(users::table)
            .values(user)
            .execute(&*conn)
            .expect("failed to insert user into db");
        
        Ok(())
    }
    
    fn update_user(&self, update: UpdateUser) -> Result<(), BackendError> {
        let user_id = update.user_id;
        let update: UpdateUserPg = update.into();
        
        let conn = self.pool.get().expect("failed to get connection from pool");
        
        let filter = users::table.filter(users::user_id.eq(user_id));
        diesel::update(filter)
            .set(update)
            .execute(&*conn)
            .expect("failed to update user in db");
        
        Ok(())
    }
    
    fn delete_user(&self, user_id: u64) -> Result<(), BackendError> {
        let conn = self.pool.get().expect("failed to get connection from pool");
        
        let filter = users::table.filter(users::user_id.eq(user_id as i32));
        diesel::delete(filter)
            .execute(&*conn)
            .expect("failed to delete user from db");
        
        Ok(())
    }
}



