
use std::ops::Deref;
use std::sync::{Mutex, RwLock};

// use models;
use crate::models::{File, NewFile, Share, User, NewUser, UpdateUser};

use serde::{Serialize, Deserialize};
use serde_json;

use super::BackendProvider;
use super::BackendError;
use crate::storage_fs::FsFileStore;

#[derive(Serialize, Deserialize, Debug)]
struct LocalShare {
    share_path: String,
    inner: Share,
}

impl Deref for LocalShare {
    type Target = Share;
    
    fn deref(&self) -> &Share {
        &self.inner
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct BackendData {
    files: Vec<File>,
    shares: Vec<LocalShare>,
    users: Vec<User>,
    next_user_id: usize,
}

pub struct LocalBackend {
    pub file_store: FsFileStore,
    db_file_path: String,
    data: RwLock<BackendData>,
    data_lock: Mutex<usize>,
}

impl LocalBackend {
    pub fn new(file_store: FsFileStore, db_file_path: String) -> Self {
        let data = BackendData {
            files: Vec::new(),
            shares: Vec::new(),
            users: Vec::new(),
            next_user_id: 0,
        };
        let data = RwLock::new(data);
        
        Self {
            file_store: file_store,
            db_file_path: db_file_path,
            data: data,
            data_lock: Mutex::new(0),
        }
    }
    
    fn save(&self) {
        let _lock = self.data_lock.lock().unwrap();
        use std::fs::File;
        
        let file = File::create(&self.db_file_path).unwrap();
        
        serde_json::to_writer_pretty(file, &*self.data.read().unwrap()).unwrap();
    }
    
    pub fn load(&mut self) {
        let _lock = self.data_lock.lock().unwrap();
        use std::fs::File;
        use std::io::ErrorKind::NotFound;
        
        let file = match File::open(&self.db_file_path) {
            Ok(file) => file,
            Err(ref err) if err.kind() == NotFound => {
                eprintln!("warning, db file not found");
                return;
            },
            Err(ref err) => panic!("{:?}", err),
        };
        
        let data = serde_json::from_reader(file).unwrap();
        
        self.data = RwLock::new(data);
    }
}

impl<'a> BackendProvider for LocalBackend {
    fn get_file_by_name(&self, user_id: u64, file_name: &str) -> Result<File, BackendError> {
        let data = self.data.read().unwrap();
        
        let file = data.files.iter().find(|f| f.user_id as u64 == user_id && f.file_name == file_name).ok_or(BackendError::NotFound)?;
        
        Ok(file.clone())
    }
    
    fn get_files(&self, user_id: u64) -> Result<Vec<File>, BackendError> {
        let data = self.data.read().unwrap();
        
        let files = data.files.iter().filter(|f| f.user_id as u64 == user_id);
        
        Ok(files.cloned().collect())
    }
    
    fn get_share(&self, share_path: &str) -> Result<Share, BackendError> {
        let data = self.data.read().unwrap();
        
        let share = data.shares.iter().find(|s| s.share_path == share_path).ok_or(BackendError::NotFound)?;
        
        // let file = self.files.iter().find(|f| f.external_id == share.file_id).ok_or(BackendError::NotFound)?;
        
        Ok(share.inner.clone())
    }
    
    fn create_file(&self, new_file: NewFile) -> Result<(), BackendError> {
        
        {
            let mut data = self.data.write().unwrap();
            
            let file: File = new_file.into();
            
            data.files.push(file);
        }
        
        self.save();
        
        Ok(())
    }
    
    fn create_share(&self, user_id: u64, file_id: &str) -> Result<String, BackendError> {
        
        let share_path = {
            let mut data = self.data.write().unwrap();
            
            let file = data.files.iter().find(|f| f.external_id == file_id).expect("create_share: failed to load file from vec").clone();
            assert!(file.user_id as u64 == user_id);
            
            let share_path = self.file_store.generate_uuid();
            
            let share = Share {
                store_path: file.store_path.clone(),
                file_name: file.file_name.clone(),
                mime_type: file.mime_type.clone(),
                login_required: false,
            };
            
            let local_share = LocalShare {
                share_path: share_path.clone(),
                inner: share,
            };
            
            data.shares.push(local_share);
            
            share_path
        };
        
        self.save();
        
        Ok(share_path)
    }
    
    fn get_user(&self, username: &str) -> Result<User, BackendError> {
        let data = self.data.read().unwrap();
        
        let user = data.users.iter().find(|u| u.username == username).ok_or(BackendError::NotFound)?;
        
        Ok(user.clone())
    }
    
    fn get_users(&self) -> Result<Vec<User>, BackendError> {
        let data = self.data.read().unwrap();
        
        let users = data.users.clone();
        
        Ok(users)
    }
    
    fn create_user(&self, new_user: NewUser) -> Result<(), BackendError> {
        {
            let mut data = self.data.write().unwrap();
            
            let user_exists = data.users.iter().find(|u| u.username == new_user.username).is_some();
            if user_exists {
                return Err(BackendError::AlreadyExists);
            }
            
            // can't get next user id based on users since deleted users' tokens are still valid
            let user_id = data.next_user_id as i32;
            data.next_user_id += 1;
            
            let new_user: User = new_user.as_user(user_id);
            
            data.users.push(new_user);
        }
        
        self.save();
        
        Ok(())
    }
    
    fn update_user(&self, update: UpdateUser) -> Result<(), BackendError> {
        {
            let mut data = self.data.write().unwrap();
            
            let user = data.users.iter_mut().find(|u| u.user_id == update.user_id).ok_or(BackendError::NotFound)?;
            
            user.update(update);
        }
        
        self.save();
        
        Ok(())
    }
    
    fn delete_user(&self, user_id: u64) -> Result<(), BackendError> {
        let user_id = user_id as i32;
        
        {
            let mut data = self.data.write().unwrap();
            
            let user_index = {
                let (user_index, _user) = data.users.iter().enumerate().find(|(_, u)| u.user_id == user_id).ok_or(BackendError::NotFound)?;
                user_index
            };
            
            data.users.swap_remove(user_index);
        }
        
        self.save();
        
        Ok(())
    }
}



