
pub mod models;
#[cfg(feature = "postgres")]
pub mod schema;
#[cfg(feature = "postgres")]
pub mod postgres;
pub mod local;

use std::ops::Deref;
use rocket::request::{self, FromRequest};
use rocket::{Request, State, Outcome};

#[derive(Debug)]
pub enum BackendError {
    NotFound,
    AlreadyExists,
    Other,
}

pub trait BackendProvider
    where Self: Send + Sync {
    
    fn get_file_by_name(&self, user_id: u64, file_name: &str) -> Result<models::File, BackendError>;
    
    fn get_files(&self, user_id: u64) -> Result<Vec<models::File>, BackendError>;
    
    fn get_share(&self, share_path: &str) -> Result<models::Share, BackendError>;
    
    fn create_file(&self, file: models::NewFile) -> Result<(), BackendError>;
    
    fn create_share(&self, user_id: u64, file_id: &str) -> Result<String, BackendError>;
    
    fn get_user(&self, username: &str) -> Result<models::User, BackendError>;
    
    fn get_users(&self) -> Result<Vec<models::User>, BackendError>;
    
    fn create_user(&self, user: models::NewUser) -> Result<(), BackendError>;
    
    fn update_user(&self, update: models::UpdateUser) -> Result<(), BackendError>;
    
    fn delete_user(&self, user_id: u64) -> Result<(), BackendError>;
}

pub struct Backend<'r> {
    pub provider: &'r BackendProvider,
}

impl<'a, 'r> FromRequest<'a, 'r> for Backend<'r> {
    type Error = ();
    
    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
        let provider = request.guard::<State<Box<BackendProvider>>>()?;
        Outcome::Success(Backend { provider: provider.inner().deref() })
    }
}

impl<'r> Deref for Backend<'r> {
    type Target = &'r BackendProvider;
    
    fn deref<'a>(&'a self) -> &'a Self::Target {
        &self.provider
    }
}
