
#[allow(unused_imports)]
use std::time::{SystemTime, Duration};
use std::borrow::Cow;

#[cfg(feature = "postgres")]
use super::schema::{files, shares, users};

#[cfg(feature = "postgres")]
pub use diesel::pg::data_types::PgInterval;

#[cfg_attr(feature = "postgres", derive(Queryable))]
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct File {
    #[cfg_attr(feature = "postgres", column_name = "id")]
    #[serde(skip_serializing)]
    #[serde(default)]
    _internal_id: i32,
    pub user_id: i32,
    pub external_id: String,
    pub store_path: String,
    pub file_name: String,
    pub mime_type: Option<String>,
    pub upload_time: SystemTime,
}

#[cfg_attr(feature = "postgres", derive(Insertable))]
#[cfg_attr(feature = "postgres", table_name = "files")]
pub struct NewFile<'a> {
    pub user_id: i32,
    pub external_id: &'a str,
    pub store_path: &'a str,
    pub file_name: &'a str,
    pub mime_type: Option<&'a str>,
}

impl<'a> From<NewFile<'a>> for File {
    fn from(new_file: NewFile) -> Self {
        Self {
            _internal_id: 0,
            user_id: new_file.user_id,
            external_id: new_file.external_id.into(),
            store_path: new_file.store_path.into(),
            file_name: new_file.file_name.into(),
            mime_type: new_file.mime_type.map(|m| m.into()),
            upload_time: SystemTime::now(),
        }
    }
}

#[cfg_attr(feature = "postgres", derive(Queryable))]
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Share {
    pub store_path: String,
    pub file_name: String,
    pub mime_type: Option<String>,
    pub login_required: bool,
}

#[cfg(feature = "postgres")]
#[derive(Insertable)]
#[table_name = "shares"]
pub struct NewShare<'a> {
    pub file_id: &'a str,
    pub share_path: &'a str,
    pub valid_duration: Option<PgInterval>,
    pub login_required: bool,
}

#[cfg_attr(feature = "postgres", derive(Queryable))]
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct User {
    #[cfg_attr(feature = "postgres", column_name = "id")]
    #[serde(skip_serializing)]
    #[serde(default)]
    _internal_id: i32,
    pub user_id: i32,
    pub username: String,
    pub password: String,
    pub admin: bool,
}

impl User {
    pub fn update(&mut self, update: UpdateUser) {
        if let Some(username) = update.username {
            self.username = username.into();
        }
        if let Some(password) = update.password {
            self.password = password.into();
        }
        if let Some(admin) = update.admin {
            self.admin = admin;
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[cfg_attr(feature = "postgres", derive(Insertable))]
#[cfg_attr(feature = "postgres", table_name = "users")]
pub struct NewUser<'a> {
    pub username: &'a str,
    pub password: Cow<'a, str>,
    pub admin: bool,
}

impl<'a> NewUser<'a> {
    pub fn as_user(self, user_id: i32) -> User {
        User {
            _internal_id: 0,
            user_id: user_id,
            username: self.username.into(),
            password: self.password.into_owned(),
            admin: self.admin,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UpdateUser<'a> {
    pub user_id: i32,
    #[serde(default)]
    pub username: Option<&'a str>,
    #[serde(default)]
    pub password: Option<Cow<'a, str>>,
    #[serde(default)]
    pub admin: Option<bool>,
}
