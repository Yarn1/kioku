
use std::path::{Path, PathBuf};
use std::fs::{OpenOptions};

use uuid::Uuid;
use uuid::v1::Context as UuidV1Context;
use time;

pub struct FsFile {
    pub file: ::std::fs::File,
    pub store_path: String,
}

pub struct FsFileStore {
    pub path: PathBuf,
    uuid_v1_context: UuidV1Context,
}

impl FsFileStore {
    pub fn new<P: AsRef<Path>>(path: P) -> Self {
        let uuid_v1_context = UuidV1Context::new(0);
        let path = PathBuf::from(path.as_ref());
        Self {
            path: path,
            uuid_v1_context: uuid_v1_context,
        }
    }
    
    pub fn generate_uuid(&self) -> String {
        let ref uuid_context = self.uuid_v1_context;
        let now = time::get_time();
        let file_uuid = Uuid::new_v1(uuid_context, now.sec as u64, now.nsec as u32, &[0, 0, 0, 0, 0, 0]).unwrap();//.unwrap().simple().to_string();
        file_uuid.to_simple().to_string()
    }
    
    pub fn create_file(&self) -> FsFile {
        let file_uuid = self.generate_uuid();
        let store_path = file_uuid;
        
        let mut path = PathBuf::new();
        path.push(&self.path);
        path.push(&store_path);
        
        let file = OpenOptions::new().write(true).create_new(true).open(&path).unwrap();
        
        FsFile {
            file: file,
            store_path: store_path,
        }
    }
}
